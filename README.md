wordlinks
=========

Django web app that tracks relationships between words, and produces various simple word puzzles based on this knowledge.

Requirements
------------

* Python 2.6
* South 0.8.4
* Django-taggit 0.10
